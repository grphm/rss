<?php
\Route::group(['middleware' => 'public'], function() {

    $this->get('rss', ['as' => 'application.rss', 'uses' => 'Controller@RSS']);
});
<?php
namespace APPLICATION_HOME\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Str;

class Controller extends ModuleController {

    public function RSS() {

        $rss = new RssController();
        $rss->setCache(60, Str::random(16));
        if(!$rss->isCached()):
            $rss->title = '';
            $rss->description = '';
            $rss->logo = asset('theme/images/header-logo.png');
            $rss->link = route('application.rss');
            $rss->setDateFormat('datetime');
            $rss->pubdate = Carbon::now();
            $rss->lang = 'ru-ru';
            $rss->setShortening(TRUE);
            $rss->setTextLimit(1000);
            foreach(News::whereLocale(\App::getLocale())->wherePublication(TRUE)->orderBy('published_start', 'DESC')->get() as $news):
                $rss->add($news->title, NULL, route('public.news.show', $news->PageUrl), $news->created_at, strip_tags($news->announce), strip_tags($news->content));
            endforeach;
        endif;
        return $rss->render('rss');
    }
}
<?php
namespace APPLICATION_HOME\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Cache;

class RssController extends ModuleController {

    private $items = [];
    public $title = '';
    public $description = '';
    public $link;
    public $logo;
    public $icon;
    public $cover;
    public $color;
    public $ga;
    public $related = FALSE;
    public $copyright = '';
    public $pubdate;
    public $lang;
    public $charset = 'utf-8';
    public $ctype = NULL;
    private $caching = 0;
    private $cacheKey = '';
    private $shortening = FALSE;
    private $shorteningLimit = 150;
    private $dateFormat = 'datetime';
    private $namespaces = [];
    private $customView = NULL;

    public function add($title, $author, $link, $pubdate, $description, $content = '', $enclosure = [], $category = '') {

        if($this->shortening):
            $description = mb_substr($description, 0, $this->shorteningLimit, 'UTF-8');
        endif;
        $this->setItem([
            'title' => $title, 'author' => $author, 'link' => $link, 'pubdate' => $pubdate, 'description' => $description,
            'content' => $content, 'enclosure' => $enclosure, 'category' => $category
        ]);
    }

    public function addItem(array $item) {

        if(array_key_exists(1, $item)):
            foreach($item as $i):
                $this->addItem($i);
            endforeach;
            return NULL;
        endif;
        if($this->shortening):
            $item['description'] = mb_substr($item['description'], 0, $this->shorteningLimit, 'UTF-8');
        endif;
        $this->setItem($item);
    }

    public function render($format = NULL, $cache = NULL, $key = NULL) {

        if($format == NULL && $this->customView == NULL):
            $format = "atom";
        endif;
        if($this->customView == NULL):
            $this->customView = $format;
        endif;
        if($cache != NULL):
            $this->caching = $cache;
        endif;
        if($key != NULL):
            $this->cacheKey = $key;
        endif;
        if($this->ctype == NULL):
            ($format == 'rss') ? $this->ctype = 'application/rss+xml' : $this->ctype = 'application/atom+xml';
        endif;
        if($this->caching > 0 && Cache::has($this->cacheKey)):
            return Response::make(Cache::get($this->cacheKey), 200, array('Content-Type' => $this->ctype.'; charset='.$this->charset));
        endif;
        if(empty($this->lang)):
            $this->lang = Config::get('application.language');
        endif;
        if(empty($this->link)):
            $this->link = Config::get('application.url');
        endif;
        if(empty($this->pubdate)):
            $this->pubdate = date('D, d M Y H:i:s O');
        endif;
        foreach($this->items as $k => $v):
            $this->items[$k]['title'] = htmlentities(strip_tags($this->items[$k]['title']));
            $this->items[$k]['pubdate'] = $this->formatDate($this->items[$k]['pubdate'], $format);
        endforeach;
        $channel = [
            'title' => htmlentities(strip_tags($this->title)), 'description' => $this->description, 'logo' => $this->logo, 'icon' => $this->icon,
            'color' => $this->color, 'cover' => $this->cover, 'ga' => $this->ga, 'related' => $this->related, 'link' => $this->link,
            'pubdate' => $this->formatDate($this->pubdate, $format), 'lang' => $this->lang, 'copyright' => $this->copyright
        ];
        $viewData = ['items' => $this->items, 'channel' => $channel, 'namespaces' => $this->getNamespaces()];
        if($this->caching > 0):
            Cache::put($this->cacheKey, View::make($this->getView($this->customView), $viewData)->render(), $this->caching);
            return Response::make(Cache::get($this->cacheKey), 200, array('Content-Type' => $this->ctype.'; charset='.$this->charset));
        elseif($this->caching == 0):
            $this->clearCache();
            return Response::make(View::make($this->getView($this->customView), $viewData), 200, array('Content-Type' => $this->ctype.'; charset='.$this->charset));
        elseif($this->caching < 0):
            $this->clearCache();
            return View::make($this->getView($this->customView), $viewData)->render();
        endif;
    }

    public static function link($url, $type = 'atom', $title = NULL, $lang = NULL) {

        if($type == 'rss'):
            $type = 'application/rss+xml';
        endif;
        if($type == 'atom'):
            $type = 'application/atom+xml';
        endif;
        if($title != NULL):
            $title = ' title="'.$title.'"';
        endif;
        if($lang != NULL):
            $lang = ' hreflang="'.$lang.'"';
        endif;
        return '<link rel="alternate"'.$lang.' type="'.$type.'" href="'.$url.'"'.$title.'>';
    }

    public function isCached() {

        if(Cache::has($this->cacheKey)):
            return TRUE;
        endif;
        return FALSE;
    }

    public function clearCache() {

        if($this->isCached()):
            Cache::forget($this->cacheKey);
        endif;
    }

    public function setCache($duration = 60, $key = "laravel-feed") {

        $this->cacheKey = $key;
        $this->caching = $duration;
        if($duration < 1):
            $this->clearCache();
        endif;
    }

    public function getView($format) {

        if($this->customView !== NULL && View::exists($this->customView)):
            return $this->customView;
        endif;
        return 'application_rss::'.$format;
    }

    public function setView($name = NULL) {

        $this->customView = $name;
    }

    public function setTextLimit($l = 150) {

        $this->shorteningLimit = $l;
    }

    public function setShortening($b = FALSE) {

        $this->shortening = $b;
    }

    private function formatDate($date, $format = 'atom') {

        if($format == "atom"):
            switch($this->dateFormat):
                case "carbon":
                    $date = date('c', strtotime($date->toDateTimeString()));
                    break;
                case "timestamp":
                    $date = date('c', $date);
                    break;
                case "datetime":
                    $date = date('c', strtotime($date));
                    break;
            endswitch;
        else:
            switch($this->dateFormat):
                case "carbon":
                    $date = date('D, d M Y H:i:s O', strtotime($date->toDateTimeString()));
                    break;
                case "timestamp":
                    $date = date('D, d M Y H:i:s O', $date);
                    break;
                case "datetime":
                    $date = date('D, d M Y H:i:s O', strtotime($date));
                    break;
            endswitch;
        endif;
        return $date;
    }

    public function addNamespace($n) {

        $this->namespaces[] = $n;
    }

    public function getNamespaces() {

        return $this->namespaces;
    }

    public function setDateFormat($format = "datetime") {

        $this->dateFormat = $format;
    }

    public function getItems() {

        return $this->items;
    }

    public function setItem($item) {

        $this->items[] = $item;
    }
}
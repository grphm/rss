<?php
namespace APPLICATION_HOME\Providers;

use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->app['view']->addNamespace('application_rss', __DIR__.'/../Resources/Views/rss');
    }

    public function register() {
    }
}
